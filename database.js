function Database() {
  var mysql = require("mysql");

  var con = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "123456789",
    database:"firstdatabase"
  });


con.connect(function (err) {
  if (err) throw err;
  console.log("Connected!");
  con.query(sql, function (err, result) {
    if (err) throw err;
    console.log("query executed");
  });
});

 		
function createDB(dbname) {
  sql = "create database " + dbname;
}

function createTable(tablename) {
  sql =
   
    "create table "+ tablename+ "(name VARCHAR(30), id int(25),college varchar(50), city varchar(15));";
}

function createRecord(tablename) {
  sql =
    "insert into " +
    tablename +
    " values ('madhuri',469,'st.Ann','chirala'),('ramya',470,'Raise','Bapatla'),('Jhansi',471,'QUIZ','ponnur'),('gowthami',472,'au','vizag')";
}

function updateTable(tablename) {
  sql = "ALTER TABLE " + tablename + " ADD COLUMN country varchar(15)";
}

function updateRecord(tablename) {
  sql = "update " + tablename + " set city ='Guntur' where id=469 ";
}

function readRecord(tablename) {
  sql = "select * from " + tablename;
}

function readDB(dbname) {
  sql = "use " + dbname;
}

function readTable(tablename) {
  sql = "desc " + tablename;
}

function deleteRecord(tablename) {
  sql = "delete from " + tablename;
}

function deleteTable(tablename) {
  sql = "drop table " + tablename;
}

let x = 5;

var sql = "";

switch (x) {
  case 1:
    // Create database

    createDB("firstdatabase");

    console.log("Database Created");

    break;

  case 2:
    // create table

    createTable("Information");

    console.log("Table Created");

    break;

  case 3:
    // create record

    createRecord("Information");

    console.log("Created Records");

    break;

  case 4:
    // update table

    updateTable("Information");

    console.log("Table updated");

    break;

  case 5:
    // update record

    updateRecord("Information");

    console.log("Record updated");

    break;

  case 6:
    // read record

    readRecord("Information");

    console.log("Reading Records");

    break;

  case 7:
    // read db

    readDB("firstdatabase");

    console.log("Reading Database");

    break;

  case 8:
    // read table

    readTable("Information");

    console.log("Reading table");

    break;

  case 9:
    // delete record

    deleteRecord("Information");

    console.log("Records deleted");

    break;

  case 10:
    // delete table

    deleteTable("Information");

    console.log("Table deleted");

    break;

  case 11:
    // delete database

    deleteDB("firstdatabase");

    console.log("Database deleted");

    break;
}
}
Database();
