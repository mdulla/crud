-- creating database

create database firstdatabase;

-- reading database

use firstdatabase;

-- deleting database

 DROP DATABASE firstdatabase;

 -- creating table 

CREATE table Information(name VARCHAR(30), id int(25),college varchar(50), city varchar(15));

-- reading table

desc Information;

-- updating table

ALTER TABLE Information  ADD COLUMN country int;

-- deleting table

drop table Information;

-- creating record

INSERT INTO Information values ('madhuri',469,'st.Ann','chirala'),('ramya',470,'Raise','Bapatla'),('Jhansi',471,'QUIZ','ponnur'),('gowthami',472,'au','vizag');

-- reading record

SELECT * FROM Information;

-- updating record

UPDATE Information   set city ='Guntur' where id=469 ;

-- deleting record

DELETE FROM Information WHERE id="469";

















